function cellfitness

%% Author: Yin Cai. Last documentation: 29.06.2016. 
%% Function: Postprocessing of CellCognition output for testing the fitness of cell lines

%% Requirements
% Analysis of the data through the complete CellCognition (tested with 1.5. version) pipeline
% including error correction (optional, recommanded) and event selection, saved as ch5 files
% Considered are experiments with H2B classification for mitotic cells, 1
% channel (current version), multiple divisions possible

% Important: due to the errors in object indexing using the "reuse hdf5"
% function in CellCognition, it is essential to process the data completely
% fresh (from the segmentation till the event selection) before using this
% function

% There should be one ch5 file for each position e.g. B09_01.ch5

% A condition.txt file summarizing the condition, reagents etc. for each position,
% (see example file in the folder) should be stored in the directory with
% the all ch5 files together. This will allow the grouping of the
% data for further analysis. The first colum should match the name of the
% ch5 files, the second and third colums can be used to store different
% chemical, siRNA, cell line etc. conditions

% Multiple experiments can be processed at once if the naming of the
% conditions, reagents are kept constant. Multiple experiments can also be
% merged later on.

%% Function
% The function calculate the duration and the frequency of a particular
% event of interest, indicated as the sequencial appearence of series of
% pre-defined classes

%% Output
% The function generates for each experiment a subfolder to store the
% outputs. For cross-experiment analysis, a folder for saving combined
% results should be chosen at the beginning.

%% Dependency: external function "uipickfiles" used for this function
% Please cite this function and all external functions properly

%% Select input directories

% Parameter for the GUI layout
options.Resize = 'on';
options.WindowStyle = 'normal';
numlines = [1 50];

inputdirlist = uipickfiles('prompt','Select the ch5 output folder for all experiments of interest');
batch_mode = questdlg('Do you want to do the complete processing, reanalyze with different groupping or only combine existing results', ...
    'Batch processing mode', ...
    'Complete process','Regroup','Combine','Complete process');
exp_num = length(inputdirlist);
if exp_num>1;
    comboutputdir = uigetdir('','Select the output directory for saving the combined analysis results (independently)');
end

%% Set up parameters
[parameterfile parameterpath] = uigetfile('*.*','Select the parameter file if exists',inputdirlist{1});
if parameterfile == 0;
    parameterdir = uigetdir('','Select the directory for saving the parameter file');
    % Get the classes defined during the CellCognition analysis
    example_dir = dir(fullfile(inputdirlist{1},'*.ch5'));
    if isequal(example_dir(1).name,'_all_positions.ch5');
        example_file = fullfile(inputdirlist{1},example_dir(2).name);
    else
        example_file = fullfile(inputdirlist{1},example_dir(1).name);
    end
    Channel_name = h5info(example_file,'/definition/feature/');
    Channel_name = Channel_name.Groups(1); % Possibly as a parameter for secondary channel based analysis
    Channel_name = Channel_name.Name;
    class_definition = h5read(example_file,[Channel_name '/object_classification/class_labels']);
    class_num = length(class_definition.label);
    % Set up the filtering criterias for each class
    dlgname = 'Delete events if class x exceeds % of the sequence';
    prompt = cell(1,class_num);
    defaultanswer = prompt;
    for clx = 1:class_num;
        clname = class_definition.name(:,clx);
        clname = cellstr(clname');
        prompt{clx} = [num2str(class_definition.label(clx)) '_' clname{1} ' '];
        defaultanswer{clx} = '100';
    end
    filter_parameters = inputdlg(prompt,dlgname,numlines,defaultanswer,options);
    
    % Set up the parameters for the test
    ch = '';
    for clx = 1:class_num;
        ch = [ch prompt{clx}];
    end
    dlgname2 = 'Please define the parameters for the event of interest';
    prompt2 = {['Duration: sum of class index [' ch ']'],'Frequency: minimum no. of frames between two events','Align on class index','Align at no. frame','name of analysis','position group by [1_position, 2_reagents, 3_condition, 4_none]'};
    defaultanswer2 = {'2,3,4','60','3','5','Full_analysis','2'};
    event_parameters = inputdlg(prompt2,dlgname2,numlines,defaultanswer2,options);
    parameterfile = fullfile(parameterdir,['parameter_' event_parameters{5} '.mat']);
    save(parameterfile,'filter_parameters','event_parameters','Channel_name','class_num','class_definition');
else
    parameterfile = fullfile(parameterpath,parameterfile);
    load(parameterfile);
end

%% Read out the class color definition
colormap_definition = zeros(class_num+1,3);
for i = 1:class_num;
    colormap_definition(i,1) = hex2dec(class_definition.color(2:3,i)');
    colormap_definition(i,2) = hex2dec(class_definition.color(4:5,i)');
    colormap_definition(i,3) = hex2dec(class_definition.color(6:7,i)');
end
colormap_definition = colormap_definition/255;

%% Perform the analysis
idx_event = [];
idx_track = [];
dur_event = [];
dur_track = [];
seq_event = [];
seq_track = [];
exist_condition = {};
for dirdx = 1:exp_num;
    inputdir = inputdirlist{dirdx};
    outputdir = fullfile(inputdir,'post_processing');
    if ~exist(outputdir,'dir');
        mkdir(outputdir);
    end
    outputfile = fullfile(outputdir,[event_parameters{5} '_data.mat']);
    %% Load and generate the groupping file
    % Load the previously processed output file
    switch batch_mode
        case{'Regroup','Combine'}
            % As outputs using different parameters can be saved in the
            % same dir, please give the name of the processing pipeline
            % name should be combined
            datafile = inputdlg({'Type the name'},'Name of the analysis to be re-analyzed or combined',numlines,event_parameters(5),options);
            datafile = fullfile(outputdir,[datafile{1} '_data.mat']);
            load(datafile);
    end
    % Generate a new groupping index matrix
    switch batch_mode
        case {'Complete process','Regroup'}
            exist_condition = {};
            if dirdx > 1;
                exist_condition = condition_up_to_now;
            end
            % Read the condition file
            groupby = str2double(event_parameters{6});
            f = fopen(fullfile(inputdir,'condition.txt'));
            h = textscan(f,'%s %s %s');
            position_name = h{1,1}(2:end);
            posnum = length(position_name);
            if groupby == 4; % Option for experiments with subpositions in each position (not tested)
                group_idx = ones(posnum,1);
            else
                group_idx = zeros(posnum,1);
                group_name = h{1,groupby}(2:end);
                for gdx = 1:length(group_name);
                    if ismember(group_name{gdx},exist_condition);
                        group_idx(gdx) = find(strcmp(group_name{gdx},exist_condition));
                    else
                        exist_condition = cat(1,exist_condition,group_name(gdx));
                        group_idx(gdx) = length(exist_condition);
                    end
                end
            end
            condition_up_to_now = exist_condition;
    end
    %% Find evens, get tracks, save outputs
    switch batch_mode
        case 'Complete process'
            rawdata = {};
            all_events_seq = [];
            all_events_timing_objidx = [];
            all_events_timing_frameidx = [];
            all_events_duration = [];
            all_events_positions = [];
            long_events_frequency = [];
            long_events_tracks = [];
            long_events_positions = [];
            for pdx = 1:posnum; % for all positions listed in condition.txt
                pdx
                filename = fullfile(inputdir,[position_name{pdx} '.ch5']);
                rootname = h5info(filename,'/sample/0/plate');
                rootname = rootname.Groups(1).Name;
                rootname = h5info(filename,[rootname '/experiment']);
                rootname = rootname.Groups(1).Name;
                rootname = h5info(filename,[rootname '/position']);
                rootname = rootname.Groups(1).Name;
                event = h5read(filename,[rootname '/object/event']);
                tracking = h5read(filename,[rootname '/object/tracking']);
                pred = h5read(filename,[rootname '/feature/' Channel_name(21:end) '/object_classification/prediction']);
                ids = h5read(filename,[rootname '/object/' Channel_name(21:end)]);
                % Generate the matrix with all tracks detected
                if pdx == 1; % Initialize sufficient large matrices for recording the track data
                    trackmat = zeros(max(ids.obj_label_id)*50,1+max(ids.time_idx));
                    trackmatl = 1+max(ids.time_idx);
                else
                    trackmat = zeros(max(ids.obj_label_id)*50,trackmatl);
                end
                l1= tracking.obj_idx1+1; % Python indexing 0 starting
                l2 = tracking.obj_idx2+1;
                i = 1;
                j = 1;
                % Get all tracks including all branches
                wait_for_process = [];
                process_idx = [];
                while i == 1;
                    p = length(process_idx);
                    if p > 0;
                        i = process_idx(1);
                        trackmat(j,:) = wait_for_process(1,:);
                        process_idx(1) = [];
                        wait_for_process(1,:) = [];
                        while i >1;
                            x = find(l1==trackmat(j,i));
                            if isempty(x)
                                i = 1;
                                j = j+1;
                            else
                                if length(x) == 1;
                                    i = i+1;
                                    trackmat(j,i) = l2(x);
                                    l1(x) = [];
                                    l2(x) = [];
                                else
                                    x = x(1);
                                    wait_for_process = cat(1,wait_for_process,trackmat(j,:));
                                    process_idx = [process_idx;i];
                                    i = i+1;
                                    trackmat(j,i) = l2(x);
                                    l1(x) = [];
                                    l2(x) = [];
                                end
                            end
                        end
                    else
                        if length(l1)>=1;
                            trackmat(j,1) = l1(1);
                            l1(1) = [];
                            trackmat(j,2) = l2(1);
                            l2(1) = [];
                            i = i+1;
                            while i >1;
                                x = find(l1==trackmat(j,i));
                                if isempty(x)
                                    i = 1;
                                    j = j+1;
                                else
                                    if length(x) == 1;
                                        i = i+1;
                                        trackmat(j,i) = l2(x);
                                        l1(x) = [];
                                        l2(x) = [];
                                    else
                                        wait_for_process = cat(1,wait_for_process,trackmat(j,:));
                                        process_idx = [process_idx;i];
                                        i = i+1;
                                        x = x(1);
                                        trackmat(j,i) = l2(x);
                                        l1(x) = [];
                                        l2(x) = [];
                                    end
                                end
                            end
                        else
                            i = 0;
                        end
                    end
                end
                trackmat(sum(trackmat>0,2)<str2double(event_parameters{4}),:) = [];
                tlength = sum(trackmat>0,2);
                tstart = unique(trackmat(:,1));
                t2 = zeros(length(tstart),size(trackmat,2));
                tlength = cat(2,tlength,(1:length(tlength))');
                % for each starting object, only keep the longest track
                for i = 1:length(tstart);
                    k = tlength(trackmat(:,1)==tstart(i),:);
                    [~,j] = max(k(:,1));
                    t2(i,:) = trackmat(k(j,2),:);
                end
                trackmat = t2;
                clear t2
                % Identify tracks with CellCognition event detected
                eventnum = max(event.obj_id)+1;
                event_startobj = zeros(1,eventnum);
                for i = 1:eventnum;
                    l = event.idx1(event.obj_id==(i-1));
                    event_startobj(i) = l(1);
                end
                event_startobj = event_startobj+1;
                eventtrack = zeros(size(trackmat,1),1);
                for i = 1:eventnum;
                    [x,y] = find(trackmat == event_startobj(i));
                    [~,maxidx] = max(y);
                    x = x(maxidx);
                    y = y(maxidx);
                    eventtrack(x,1) = eventtrack(x,1)+1;
                    eventtrack(x,eventtrack(x,1)+1) = y;
                end
                % Keep only tracks with event(s)
                trackmat(eventtrack(:,1)==0,:) = [];
                eventtrack(eventtrack(:,1)==0,:) = [];
                for i = 1:size(eventtrack,1);
                    eventtrack(i,2:(eventtrack(i,1)+1)) = sort(eventtrack(i,2:(eventtrack(i,1)+1)));
                end
                tracklength = sum(trackmat>0,2);
                % Quality control of the selected tracks
                trackclass = zeros(size(trackmat));
                for i = 1:size(trackclass,1);
                    for j = 1:size(trackclass,2);
                        if trackmat(i,j) ~= 0;
                            trackclass(i,j) = pred.label_idx(trackmat(i,j))+1;
                        end
                    end
                end
                trackqc = ones(size(trackmat,1),1);
                for i = 1:class_num;
                    class_tot = sum(trackclass==i,2);
                    trackqc((class_tot./tracklength) > (str2double(filter_parameters{i})/100)) = 0;
                end
                trackmat(trackqc==0,:) = [];
                tracklength(trackqc==0) = [];
                trackclass(trackqc==0,:) = [];
                eventtrack(trackqc==0,:) = [];

                % Chop and analyze each detected event and perform
                % alignment
                eventmat = zeros(sum(eventtrack(:,1)),str2double(event_parameters{2}));
                eventobj = zeros(sum(eventtrack(:,1)),1);
                eventframe = eventobj;
                eventduration = eventobj;
                eventtrack2 = eventtrack;
                event_definition = parameter_duration(event_parameters{1});
                edx = 0;
                for i = 1:size(eventtrack,1);
                    for j = 1:(size(eventtrack,2)-1);
                        if eventtrack(i,j+1) ~= 0;
                            edx = edx + 1;
                            binary_event = ismember(trackclass(i,:),event_definition);
                            k = eventtrack(i,j+1);
                            if binary_event(eventtrack(i,j+1)) == 0;
                                k_before = find(binary_event(k:-1:1)==1,1);
                                if isempty(k_before);
                                    k_before = k;
                                end
                                k_after = find(binary_event(k:tracklength(i))==1,1);
                                if isempty(k_after)
                                    k_after = tracklength(i) - k + 1;
                                end
                                if k_before < k_after;
                                    k = k - k_before + 1;
                                else
                                    k = k + k_after - 1;
                                end
                            end
                            if k > 1;
                                event_startframe = find(binary_event((k-1):-1:1)==0,1);
                                if isempty(event_startframe)
                                    event_startframe = k;
                                end
                                event_startframe = k - event_startframe + 1;
                            else
                                event_startframe = 1;
                            end
                            if k < tracklength(i);
                                event_endframe = find(binary_event((k+1):tracklength(i))==0,1);
                                if isempty(event_endframe)
                                    event_endframe = tracklength(i)-k+1;
                                end
                                event_endframe = k + event_endframe - 1;
                            else
                                event_endframe = tracklength(i);
                            end
                            eventduration(edx) = event_endframe - event_startframe + 1;
                            k_align = find(trackclass(i,event_startframe:event_endframe)==str2double(event_parameters{3}),1);
                            if isempty(k_align);
                                k_align = find(ismember(trackclass(i,event_startframe:event_endframe),event_definition(event_definition>str2double(event_parameters{3})))==1,1);
                                if isempty(k_align);
                                    k_align = find(ismember(trackclass(i,event_startframe:event_endframe),event_definition(event_definition<str2double(event_parameters{3})))==1,1);
                                    if isempty(k_align);
                                        k_align = 1;
                                    end
                                end
                            end
                            k_align = k_align + event_startframe - 1;
                            eventobj(edx) = trackmat(i,k_align);
                            eventframe(edx) = ids.time_idx(eventobj(edx));
                            eventtrack2(i,j+1) = k_align;
                            if k_align < str2double(event_parameters{4});
                                addframe = str2double(event_parameters{4})-k_align;
                                eventmat(edx,:) = cat(2,zeros(1,addframe),trackclass(i,1:(str2double(event_parameters{2})-addframe)));
                            else
                                if (k_align+str2double(event_parameters{2})-str2double(event_parameters{4}))>tracklength(i);
                                    addframe = k_align+str2double(event_parameters{2})-str2double(event_parameters{4})-tracklength(i);
                                    eventmat(edx,:) = cat(2,trackclass(i,(k_align-str2double(event_parameters{4})+1):tracklength(i)),ones(1,addframe));
                                else
                                    eventmat(edx,:) = trackclass(i,(k_align-str2double(event_parameters{4})+1):(k_align-str2double(event_parameters{4})+str2double(event_parameters{2})));
                                end
                            end
                        end
                    end
                end

                % Calculate the frequency quantities
                multievent_idx = (eventtrack2(:,1)>1);
                multievent_duration = zeros(size(multievent_idx));
                multievent_tracks = zeros(size(trackclass,1),size(trackclass,2));
                for i = 1:length(multievent_idx);
                    if multievent_idx(i) == 0;
                        % If only one event detected, take the longer none
                        % event subsession of the track (start -> event or
                        % event -> end)
                        if tracklength(i)-eventtrack2(i,2) + 1 >= eventtrack2(i,2);
                            multievent_tracks(i,1:(tracklength(i)-eventtrack2(i,2)+1)) = trackclass(i,eventtrack2(i,2):tracklength(i));
                            multievent_duration(i) = tracklength(i)-eventtrack2(i,2) + 1;
                        else
                            multievent_tracks(i,1:eventtrack2(i,2)) = trackclass(i,1:eventtrack2(i,2));
                            multievent_duration(i) = eventtrack2(i,2);
                        end
                    else
                        % if multiple events, then take the mean of the
                        % track's between-event-duration
                        multievent_duration(i) = mean(diff(eventtrack2(i,2:(eventtrack2(i,1)+1))));
                        multievent_tracks(i,1:(tracklength(i)-eventtrack2(i,2)+1)) = trackclass(i,eventtrack2(i,2):tracklength(i));
                    end
                end
                multievent_tracks = multievent_tracks(:,1:(size(trackclass,2)-str2double(event_parameters{4})+1));
                % Summarize the output
                % Summarize the raw data
                rawdata{pdx,1} = filename;
                rawdata{pdx,2} = rootname;
                rawdata{pdx,3} = Channel_name(21:end);
                rawdata{pdx,4} = pdx;
                rawdata{pdx,5} = trackmat;
                rawdata{pdx,6} = trackclass;
                rawdata{pdx,7} = tracklength;
                rawdata{pdx,8} = eventtrack;
                rawdata{pdx,9} = eventtrack2;
                % Summarize the event matrix
                all_events_seq = cat(1,all_events_seq,eventmat);
                all_events_timing_objidx = cat(1,all_events_timing_objidx,eventobj);
                all_events_timing_frameidx = cat(1,all_events_timing_frameidx,eventframe);
                all_events_duration = cat(1,all_events_duration,eventduration);
                all_events_positions = cat(1,all_events_positions,pdx*ones(size(eventobj)));
                long_events_frequency = cat(1,long_events_frequency,cat(2,multievent_idx,multievent_duration));
                long_events_tracks = cat(1,long_events_tracks,multievent_tracks);
                long_events_positions = cat(1,long_events_positions,pdx*ones(size(multievent_idx)));
            end
            headname = {'filename','pathname','channel','position_idx','selected_tracks_objidx','selected_tracks_class','track_length','event_position_detected','event_position_aligned'};
            event_track_data = cell2struct(rawdata,headname,2);
    end
    switch batch_mode
        case {'Complete process','Regroup'}
            % Generate output for each position
            event_stat = {};
            headname2 = {'condition','number_of_positions','number_of_events','averaged_event_duration','std_event_duration', 'median_event_duration', 'percentile_25', 'percentile_75', 'p_val_ttest', 'number_of_tracks','prop_of_multiple_event','averaged_duration_between_events'};
            group_event = zeros(size(all_events_positions,1),length(exist_condition));
            group_track = zeros(size(long_events_positions,1),length(exist_condition));
            
            % find HK_WT
            iWT = -1;
            for i = 1:length(exist_condition)
               if strcmp(exist_condition{i}, 'HK_WT')
                   iWT = i
                   break
               end
            end
            if iWT > 0
                g = find(group_idx==iWT);
                event_groupidx = ismember(all_events_positions,g);
                WTevents = all_events_duration(event_groupidx>0);
            end
            
            for i = 1:length(exist_condition);
                % Calculate the statistics
                event_stat{i,1} = exist_condition{i};
                event_stat{i,2} = sum(group_idx==i);
                g = find(group_idx==i);
                if ~isempty(g)
                    event_groupidx = ismember(all_events_positions,g);
                    group_event(:,i) = event_groupidx;
                    event_stat{i,3} = sum(event_groupidx);
                    event_stat{i,4} = mean(all_events_duration(event_groupidx>0));
                    event_stat{i,5} = std(all_events_duration(event_groupidx>0));
                    event_stat{i,6} = median(all_events_duration(event_groupidx>0));
                    event_stat{i,7} = prctile(all_events_duration(event_groupidx>0), 25);
                    event_stat{i,8} =  prctile(all_events_duration(event_groupidx>0), 75);
                    if iWT > 0
                        [h, p] = ttest2(all_events_duration(event_groupidx>0), WTevents);
                    else
                        p = -1;
                    end
                    event_stat{i,9} = p
                    long_groupidx = ismember(long_events_positions,g);
                    group_track(:,i) = long_groupidx;
                    submat_fr = long_events_frequency(long_groupidx>0,:);
                    longseq = ((submat_fr(:,1)+(submat_fr(:,2)>str2double(event_parameters{2})))>0);
                    event_stat{i,10} = sum(long_groupidx);
                    event_stat{i,11} = sum(submat_fr(:,1))/sum(longseq);
                    event_stat{i,12} = mean(submat_fr(submat_fr(:,1)>0,2));
                    % Generate the tif files for visualization
                    singleevent = cat(2,all_events_duration(event_groupidx>0),all_events_seq(event_groupidx>0,:));
                    singleevent = sortrows(singleevent,1);
                    singleevent = singleevent(:,2:end);
                    singleevent(singleevent==0) = class_num+1;
                    multitif = cat(2,submat_fr(:,2),long_events_tracks(long_groupidx>0,:));
                    multitif = multitif(longseq>0,:);
                    multitif = sortrows(multitif,1);
                    multitif = multitif(:,2:end);
                    multitif(multitif==0) = class_num+1;
                    if ~isempty(singleevent);
                        output_tif = fullfile(outputdir,[event_parameters{5} '_' exist_condition{i} '_single_events.tif']);
                        % change the image ration to 3:1
                        size_TE = size(singleevent);
                        singleevent = repmat(singleevent,[3,1]);
                        singleevent = singleevent(:);
                        singleevent = reshape(singleevent,size_TE(1),size_TE(2)*3);
                        imwrite(singleevent,colormap_definition,output_tif,'tif')
                    end
                    if ~isempty(multitif);
                        output_tif = fullfile(outputdir,[event_parameters{5} '_' exist_condition{i} '_multiple_events.tif']);
                        % change the image ration to 3:1
                        size_TE = size(multitif);
                        multitif = repmat(multitif,[3,1]);
                        multitif = multitif(:);
                        multitif = reshape(multitif,size_TE(1),size_TE(2)*3);
                        imwrite(multitif,colormap_definition,output_tif,'tif')
                    end
                end
                disp('tif generated')
            end
            event_statistic = cell2struct(event_stat,headname2,2);
            save(outputfile,'exist_condition','group_idx','event_track_data','all_events_seq','all_events_timing_objidx','all_events_timing_frameidx','all_events_duration','all_events_positions','long_events_frequency','long_events_tracks','long_events_positions','event_statistic','parameterfile','group_event','group_track');
            %% write event statistic to a tab_delimited file
            fnames = fieldnames(event_statistic);
            [mdir, basename, ext] = fileparts(outputfile);
            xlswrite(fullfile(mdir, [basename, '_event_statistic.xls']), [fnames'; transpose(struct2cell(event_statistic))]);
    end
    idx_event = cat(1,idx_event,group_event);
    idx_track = cat(1,idx_track,group_track);
    if dirdx == 1;
        group_exp = (sum(idx_event,1)>0);
        group_pos = zeros(1,size(group_event,2));
        for i = 1:size(group_event,2);
            group_pos(i) = event_statistic(i).number_of_positions;
        end
    else
        group_exp = group_exp + (sum(idx_event,1)>0);
        for i = 1:size(group_event,2);
            group_pos(i) = group_pos(i) + event_statistic(i).number_of_positions;
        end
    end
    dur_event = cat(1,dur_event,all_events_duration);
    dur_track = cat(1,dur_track,long_events_frequency);
    seq_event = cat(1,seq_event,all_events_seq);
    seq_track = cat(1,seq_track,long_events_tracks);
end
if exp_num > 1;
    event_stat = {};
    headname3 = {'condition','number_of_experiments','number_of_positions','number_of_events','averaged_event_duration','number_of_tracks','prop_of_multiple_event','averaged_duration_between_events'};
    for i = 1:length(exist_condition);
        % Calculate the statistics
        event_stat{i,1} = exist_condition{i};
        event_stat{i,2} = group_exp(i);
        event_stat{i,3} = group_pos(i);
        event_stat{i,4} = sum(idx_event(:,i));
        event_stat{i,6} = sum(idx_track(:,i));
        if event_stat{i,4} > 0;
            event_stat{i,5} = mean(dur_event(idx_event(:,i)>0));
            output_tif = fullfile(comboutputdir,[event_parameters{5} '_' exist_condition{i} '_single_events.tif']);
            singleevent = cat(2,dur_event(idx_event(:,i)>0),seq_event(idx_event(:,i)>0,:));
            singleevent = sortrows(singleevent,1);
            singleevent = singleevent(:,2:end);
            singleevent(singleevent==0) = class_num+1;
            % change the image ration to 3:1
            size_TE = size(singleevent);
            singleevent = repmat(singleevent,[3,1]);
            singleevent = singleevent(:);
            singleevent = reshape(singleevent,size_TE(1),size_TE(2)*3);
            imwrite(singleevent,colormap_definition,output_tif,'tif')
        end
        if event_stat{i,6} > 0;
            submat_fr = dur_track(idx_track(:,i)>0,:);
            longseq = ((submat_fr(:,1)+(submat_fr(:,2)>str2double(event_parameters{2})))>0);
            event_stat{i,7} = sum(submat_fr(:,1))/sum(longseq);
            event_stat{i,8} = mean(submat_fr(submat_fr(:,1)>0,2));
            multitif = cat(2,submat_fr(:,2),seq_track(idx_track(:,i)>0,:));
            multitif = multitif(longseq>0,:);
            multitif = sortrows(multitif,1);
            multitif = multitif(:,2:end);
            multitif(multitif==0) = class_num+1;
            if ~isempty(multitif);
                output_tif = fullfile(comboutputdir,[event_parameters{5} '_' exist_condition{i} '_multiple_events.tif']);
                % change the image ration to 3:1
                size_TE = size(multitif);
                multitif = repmat(multitif,[3,1]);
                multitif = multitif(:);
                multitif = reshape(multitif,size_TE(1),size_TE(2)*3);
                imwrite(multitif,colormap_definition,output_tif,'tif')
            end
        end
    end
    event_statistic = cell2struct(event_stat,headname3,2);
    outputfile = fullfile(comboutputdir,[event_parameters{5} '_data.mat']);
    save(outputfile,'exist_condition','idx_event','idx_track','dur_event','dur_track','seq_event','seq_track','event_statistic','parameterfile');
end
end

function duration_of = parameter_duration(list_class)

duration_of_comma = find(list_class==',');
if isempty(list_class);
    duration_of = [];
else
    duration_classnum = length(duration_of_comma)+1;
    if duration_classnum == 1;
        duration_of = str2double(list_class);
    else
        duration_of = zeros(duration_classnum,1);
        duration_of(1) = str2double(list_class(1:(duration_of_comma(1)-1)));
        for comma_idx = 2:(duration_classnum-1);
            duration_of(comma_idx) = str2double(list_class((duration_of_comma(comma_idx-1)+1):(duration_of_comma(comma_idx)-1)));
        end
        duration_of(end) = str2double(list_class((duration_of_comma(end)+1):end));
    end
end

end