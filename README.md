# CellCognition  

[CellCognition](http://www.cellcognition.org/) is a freely available software. It is used to automatically track single cells through the cell cycle and classify them into interphase and mitotic stages.
Here, we provide example settings routinely used with CeCogAnalyzer 1.5.2 to create tracks of cells going through mitosis as hdf5 output with the aim to determine mitotic timing. The settings are based on images acquired on a ImageXpress Micro XLS Widefield High-Content Analysis System (Molecular Devices; images acquired every 5 min in Cy5 channel for 24 h) but can be adapted to any other automated wide-field imaging system. 


# Cellfitness

Matlab function for postprocessing of [CellCognition](http://www.cellcognition.org/) output for measuring cell cycle timing and statistics of cell lines. The function calculates the duration and the frequency of a particular event of interest, indicated as the sequencial appearence of series of
pre-defined classes. The GUI is set up from the function **uipickfiles**. At start-up a GUI opens that allows the user to choose the hdf5 directory for further processing. 

For further details see the [wiki](https://git.embl.de/nwalther/Cecog_HMM_PostProcessing/wikis/home).

## Author
Yin Cai, EMBL, Heidelberg, 2015

Modified by Antonio Politi, EMBL Heidelberg, 2016 (additional statistics)

Documented by Nike Walther, EMBL Heidelberg, 2017